public class Criminal {
    protected String firstName;
    protected String lastName;
    protected int yearOfBirth;
    private int yearOfCrime;
    private boolean crimeIsOrganized;
    public Criminal(String firstName, String lastName, int yearOfBirth, int yearOfCrime, boolean crimeIsOrganized) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.yearOfBirth = yearOfBirth;
        this.yearOfCrime = yearOfCrime;
        this.crimeIsOrganized = crimeIsOrganized;
    }
    protected String criminalIsCaught(boolean isCaught){
        return isCaught ? "Criminal " + this.firstName + this.lastName + " is caught" : "Continue an investigation";
    }
    protected void makeCrime(){
        System.out.println("Make a crime");
    }
    public int calculateAge(){
        return this.yearOfCrime-this.yearOfBirth;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public int getYearOfBirth() {
        return yearOfBirth;
    }
    public int getYearOfCrime() {
        return yearOfCrime;
    }
    public boolean isCrimeIsOrganized() {
        return crimeIsOrganized;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }
    public void setYearOfCrime(int yearOfCrime) {
        this.yearOfCrime = yearOfCrime;
    }
    public void setCrimeIsOrganized(boolean crimeIsOrganized) {
        this.crimeIsOrganized = crimeIsOrganized;
    }
}
