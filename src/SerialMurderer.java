import java.util.List;

public class SerialMurderer extends Murderer {
    private int amountOfVictims;
    private List<String> victims;
    public SerialMurderer(String firstName, String lastName, int yearOfBirth, int yearOfCrime, boolean crimeIsOrganized, String weapon, int amountOfVictims, List<String> victims) {
        super(firstName, lastName, yearOfBirth, yearOfCrime, crimeIsOrganized, weapon);
         this.amountOfVictims = amountOfVictims;
         this.victims = victims;
    }
    public void haveDisorders(){
        System.out.println("To have personality disorders");
    }
    @Override
    public void makeCrime(){
        System.out.println("Commit serial murder");
    }
    @Override
    protected void toKill(){
        for(String vic:victims){
            System.out.println("To kill "+vic);
        }
    }
    public List<String> getVictims() {
        return victims;
    }
    public void setVictims(String victim) {
        victims.add(victim);
    }
    public int getAmountOfVictims() {
        return amountOfVictims;
    }
    public void setAmountOfVictims(int amountOfVictims) {
        this.amountOfVictims = amountOfVictims;
    }
}
