public class Murderer extends Criminal {
    private String weapon;
    public Murderer(String firstName, String lastName, int yearOfBirth, int yearOfCrime, boolean crimeIsOrganized, String weapon) {
        super(firstName, lastName, yearOfBirth, yearOfCrime, crimeIsOrganized);
        this.weapon = weapon;
    }

    @Override
    public void makeCrime() {
        System.out.println("Commit murder");
    }
    protected void toKill(String victim){
        System.out.println("To kill "+victim);
    }
    protected void toKill(){
        System.out.println("To kill somebody");
    }
    public String getWeapon() {
        return weapon;
    }
    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }
}
