import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Creation of victims list
        List<String> vic = new ArrayList<>();
        vic.add("Sara Moray");
        vic.add("Dina Green");
        vic.add("Nina Johns");
        Criminal criminal = new Criminal("Dave","Kubrick",1980,2000,true);
        Murderer murderer = new Murderer("Clint","Simpson",1950,1988,false,"Knife");
        SerialMurderer serialMurderer = new SerialMurderer("Will","Gardner",1995,2015,true,"gun",3,vic);
        // Accessing a private parent attribute: causes compiler error
        // System.out.println(serialMurderer.weapon);
        // Accessing protected parent attribute
        System.out.println(murderer.firstName+" "+murderer.lastName+" "+murderer.yearOfBirth);
        // Calling a non-overridden parent method
        serialMurderer.toKill("Debora Linn");
        serialMurderer.toKill();
        // Calling method of first parent
        System.out.println(serialMurderer.criminalIsCaught(true));
    }
}
