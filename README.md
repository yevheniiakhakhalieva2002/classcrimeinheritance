# Criminal record project

Criminal record is a project for classification and information management of criminals. In Criminal record project are stored name
and year of birth of criminal, also year of crime and whether this crime is organized or not. If a criminal is a murderer then is
stored information about the weapon that the criminal used. If a criminal is a serial murderer then are added amount of his victims
and list with their names. Besides, there're functions that print and set data, calculate age of criminal in which the crime was made,
decide the criminal is caught or need to continue investigation, show which murderer killed which victim and other additional methods.

## Visuals

![](ClassCriminal.png)

## Usage

```java
System.out.println(murderer.getFirstName()+" "+murderer.getLastName()+" "+murderer.getYearOfBirth()); // Clint Simpson 1950
serialMurderer.toKill("Debora Linn"); // To kill Debora Linn
System.out.println(serialMurderer.criminalIsCaught(true)); // Criminal Will Gardner is caught
```



